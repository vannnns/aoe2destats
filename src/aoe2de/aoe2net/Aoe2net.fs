namespace aoe2net

open FSharp.Data
open FSharp.Data.JsonExtensions
open FSharp.Data.HttpRequestHeaders

module Types =

    type Player =
        { Name: string
          TeamNum: int
          Elo: int
          Civ: string }

    type Team = { Num: int; Players: Player list }

    type Match = { Teams: Team list }

    type SteamId = SteamId of string

module API =

    open Types

    let getLastMatch ((SteamId steamId): SteamId): Match option =
        let url =
            "https://aoe2.net/api/player/lastmatch?game=aoe2de&steam_id="
            + steamId

        let httpResponse =
            Http.Request(url, silentHttpErrors = true)

        match httpResponse.StatusCode with
        | 200 ->
            match httpResponse.Body with
            | Text content ->
                let response = content |> JsonValue.Parse

                let jsonPlayers =
                    response?last_match?players.AsArray()
                    |> Array.toList

                let parseRating rating =
                    if rating <> JsonValue.Null then rating.AsInteger() else 0

                let allPlayers =
                    List.map (fun p ->
                        { Name = p?name.AsString()
                          TeamNum = p?team.AsInteger()
                          Elo = parseRating p?rating
                          Civ = p?civ.AsString() }) jsonPlayers

                let rec addPlayer (teams: Team list) (player: Player) =
                    match teams with
                    | [] ->
                        [ { Num = player.TeamNum
                            Players = [ player ] } ]
                    | t :: ts ->
                        if (t.Num = player.TeamNum) then
                            { Num = t.Num
                              Players = player :: t.Players }
                            :: ts
                        else
                            t :: (addPlayer ts player)

                Some { Teams = List.fold addPlayer [] allPlayers }
            | _ -> None
        | _ -> None
