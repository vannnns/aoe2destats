module aoe2de.App

open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open aoe2net.Types
open aoe2net.API

// ---------------------------------
// Models
// ---------------------------------

type ViewModel = { MatchInfos: Match option }

// ---------------------------------
// Views
// ---------------------------------

module Views =
    open GiraffeViewEngine

    let layout (content: XmlNode list) =
        html []
            [ head []
                  [ title [] [ encodedText "aoe2de" ]
                    link
                        [ _rel "stylesheet"
                          _type "text/css"
                          _href "/assets/css/main.css" ]
                    link
                        [ _rel "stylesheet"
                          _type "text/css"
                          _href "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.2/css/bulma.min.css" ] ]
              body [] content ]

    let partial teamRenderer =
        h1 []
            [ encodedText "Last or current match infos"
              teamRenderer () ]

    // <div class="level-left">
    //       <div class="player">
    //         <div>SP* Van'S</div>
    //         <div>1153</div>
    //       </div>
    //     </div>
    let renderPlayer { Name = playerName; Elo = elo } =
        div [ _class "level-left" ]
            [ div [ _class "player has-text-weight-bold" ]
                  [ div [ _class "is-size-3" ] [ encodedText playerName ]
                    div [] [ encodedText (elo.ToString()) ] ] ]

    let renderCiv civName =
        div [ _class "level-left" ] [ div [ _class "civ" ] [ div [] [ encodedText ("Civ: " + civName) ] ] ]

    let renderPlayerSlot player =
        div [ _class "level has-background-warning player-slot" ]
            [ renderPlayer player
              renderCiv player.Civ ]

    let renderTeam (t: Team) =
        div [ _class "column team" ]
            [ div []
                  ([ p [ _class "is-size-2" ] [ encodedText ("Team " + t.Num.ToString()) ] ]
                   @ (List.map renderPlayerSlot t.Players)) ]

    // <section class="hero is-primary">
    //   <div class="hero-body">
    let renderMatch (m: Match) =
        div [ _class "container" ] [ div [ _class "columns" ] (List.map renderTeam m.Teams) ]

    let index (viewModel: ViewModel) =
        [ section [ _class "hero is-primary" ]
              [ div [ _class "hero-body" ]
                    [ div [ _class "container" ]
                          [ h1 [ _class "title" ] [ encodedText "Match info" ]
                            h1 [ _class "subtitle" ] [ encodedText "Last or current" ] ] ] ]
          match viewModel.MatchInfos with
          | Some m -> renderMatch m
          | None -> encodedText "No match found" ]
        |> layout

// ---------------------------------
// Web app
// ---------------------------------

let vansSteamId = "76561197961948532"

let indexHandler (steamId: string) =
    let matchI = getLastMatch (SteamId steamId)
    let model = { MatchInfos = matchI }
    let view = Views.index model
    htmlView view


let webApp =
    choose
        [ GET
          >=> choose
                  [ route "/"
                    >=> warbler (fun _ -> indexHandler vansSteamId)
                    routef "/lastMatch/%s" indexHandler ]
          setStatusCode 404 >=> text "Ooopsy, not Found ^o^" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex: Exception) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse
    >=> setStatusCode 500
    >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder: CorsPolicyBuilder) =
    builder.WithOrigins("http://139.162.138.94:80").AllowAnyMethod().AllowAnyHeader()
    |> ignore

let configureApp (app: IApplicationBuilder) =
    let env =
        app.ApplicationServices.GetService<IHostingEnvironment>()

    (match env.IsDevelopment() with
     | true -> app.UseDeveloperExceptionPage()
     | false -> app.UseGiraffeErrorHandler errorHandler).UseCors(configureCors).UseStaticFiles().UseGiraffe(webApp)

let configureServices (services: IServiceCollection) =
    services.AddCors() |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder: ILoggingBuilder) =
    builder.AddFilter(fun l -> l.Equals LogLevel.Error).AddConsole().AddDebug()
    |> ignore

[<EntryPoint>]
let main _ =
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot = Path.Combine(contentRoot, "WebRoot")
    WebHostBuilder().UseKestrel().UseContentRoot(contentRoot).UseWebRoot(webRoot).UseUrls("http://*:80")
        .Configure(Action<IApplicationBuilder> configureApp).ConfigureServices(configureServices)
        .ConfigureLogging(configureLogging).Build().Run()
    0
